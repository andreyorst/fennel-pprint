(local pp (require :pp))

(macro assert-eq [a b]
  `(let [a# ,a
         b# ,b]
     (assert (= a# b#) (.. "equality assertion failed\n\nleft:\n" a# "\n\nright:\n" b#))))

;; base
(assert-eq (pp 123) "123")
(assert-eq (pp 2.4) "2.4")
(assert-eq (pp "ваыв") "\"ваыв\"")
(assert-eq (pp [] {:empty-as-sequence? true}) "[]")
(assert-eq (pp []) "{}")
(assert-eq (pp [1 2 3]) "[1 2 3]")
(assert-eq (pp [0 1 2 3 4 5 6 7 8 9 10]) "[0\n 1\n 2\n 3\n 4\n 5\n 6\n 7\n 8\n 9\n 10]")
(assert-eq (pp {:a 1 "a b" 2}) "{:a 1 \"a b\" 2}")

;; nesting
(assert-eq (pp [{}]) "[{}]")
(assert-eq (pp {[{}] []}) "{[{}] {}}")
(assert-eq (pp {[[]] {[[]] [[[]]]}} {:empty-as-sequence? true}) "{[[]] {[[]] [[[]]]}}")
(assert-eq (pp [1 2 [3 4]]) "[1\n 2\n [3 4]]")
(assert-eq (pp {[1] [2 [3]] :data {4 {:data 5} 6 [0 1 2 3]}}
               {:sequential-length 3})
           "{:data [{:data 5}\n        [0\n         1\n         2\n         3]]\n [1] [2\n      [3]]}")
(assert-eq (pp {{:b 2} {:c 3 :d 4} {:a 1} {:b 2 :c 3}})
           "{{:a 1} {:b 2 :c 3}\n {:b 2} {:c 3 :d 4}}")
(assert-eq (pp [{:aaa [1 2 3]}] {:sequential-length 2})
           "[{:aaa [1\n        2\n        3]}]")

;; Unicode length
(assert-eq (pp {[1] [2 [3]] :ваыв {4 {:ваыв 5} 6 [0 1 2 3]}}
               {:sequential-length 3 :utf8? true})
           "{\"ваыв\" [{\"ваыв\" 5}\n         [0\n          1\n          2\n          3]]\n [1] [2\n      [3]]}")
;; the next one may look incorrect in some editors, but is actually correct
(assert-eq (pp {:ǍǍǍ {} :ƁƁƁ {:ǍǍǍ {} :ƁƁƁ {}}} {:utf8? true})
           "{\"ƁƁƁ\" {\"ƁƁƁ\" {}\n        \"ǍǍǍ\" {}}\n \"ǍǍǍ\" {}}")


;; cycles
(local t1 {})
(tset t1 :t1 t1)
(assert-eq (pp t1) "@1{:t1 @1{...}}")

(local t1 {})
(tset t1 t1 t1)
(assert-eq (pp t1) "@1{@1{...} @1{...}}")

(local v1 [])
(table.insert v1 v1)
(assert-eq (pp v1) "@1[@1[...]]")

(local t1 {})
(local t2 {:t1 t1})
(tset t1 :t2 t2)
(assert-eq (pp t1) "@1{:t2 {:t1 @1{...}}}")

(local t1 {:a 1 :c 2})
(local v1 [1 2 3])
(tset t1 :b v1)
(table.insert v1 2 t1)
(assert-eq (pp t1) "@1{:a 1\n   :b [1\n       @1{...}\n       2\n       3]\n   :c 2}")

(local v1 [1 2 3])
(local v2 [1 2 v1])
(local v3 [1 2 v2])
(table.insert v1 v2)
(table.insert v1 v3)

(assert-eq (pp v1) "@1[1\n   2\n   3\n   @2[1\n      2\n      @1[...]]\n   [1\n    2\n    @2[...]]]")

(local v1 [])
(table.insert v1 v1)
(assert-eq (pp v1 {:detect-cycles? false :one-line? true :depth 10})
           "[[[[[[[[[[...]]]]]]]]]]")

(local t1 [])
(tset t1 t1 t1)
(assert-eq (pp t1 {:detect-cycles? false :one-line? true :depth 4})
           "{{{{...} {...}} {{...} {...}}} {{{...} {...}} {{...} {...}}}}")

(local v1 [])
(local v2 [v1])
(local v3 [v1 v2])
(local v4 [v2 v3])
(local v5 [v3 v4])
(local v6 [v4 v5])
(local v7 [v5 v6])
(local v8 [v6 v7])
(local v9 [v7 v8])
(local v10 [v8 v9])
(local v11 [v9 v10])

(table.insert v1 v2)
(table.insert v1 v3)
(table.insert v1 v4)
(table.insert v1 v5)
(table.insert v1 v6)
(table.insert v1 v7)
(table.insert v1 v8)
(table.insert v1 v9)
(table.insert v1 v10)
(table.insert v1 v11)
(assert-eq (pp v1)
           "@1[@2[@1[...]]\n   @3[@1[...]\n      @2[...]]\n   @4[@2[...]\n      @3[...]]\n   @5[@3[...]\n      @4[...]]\n   @6[@4[...]\n      @5[...]]\n   @7[@5[...]\n      @6[...]]\n   @8[@6[...]\n      @7[...]]\n   @9[@7[...]\n      @8[...]]\n   @10[@8[...]\n       @9[...]]\n   [@9[...]\n    @10[...]]]")

(table.insert v2 v11)
(assert-eq (pp v1)
           "@1[@2[@1[...]\n      @3[@4[@5[@6[@7[@1[...]\n                     @2[...]]\n                  @8[@2[...]\n                     @7[...]]]\n               @9[@8[...]\n                  @6[...]]]\n            @10[@9[...]\n                @5[...]]]\n         @11[@10[...]\n             @4[...]]]]\n   @7[...]\n   @8[...]\n   @6[...]\n   @9[...]\n   @5[...]\n   @10[...]\n   @4[...]\n   @11[...]\n   @3[...]]")

;; __fennelview tests
(fn pp-list [x pp opts indent]
  (icollect [i v (ipairs x)]
    (let [v (pp v opts (+ 1 indent) true)]
      (if (= i 1) (.. "(" v)
          (= i (length x)) (.. " " v ")")
          (.. " " v)))))

(local l1 (setmetatable [1 2 3] {:__fennelview pp-list}))

(assert-eq (pp l1) "(1\n 2\n 3)")
(assert-eq (pp [l1]) "[(1\n  2\n  3)]")
(assert-eq (pp [1 l1 2]) "[1\n (1\n  2\n  3)\n 2]")
(assert-eq (pp [[1 l1 2]]) "[[1\n  (1\n   2\n   3)\n  2]]")
(assert-eq (pp {:abc [l1]}) "{:abc [(1\n        2\n        3)]}")
(assert-eq (pp l1 {:one-line? true}) "(1 2 3)")
(assert-eq (pp [l1] {:one-line? true}) "[(1 2 3)]")
(assert-eq (pp {:abc [l1]} {:one-line? true}) "{:abc [(1 2 3)]}")

(local l2 (setmetatable ["a" "a b" [1 2 3] {:a l1 :b []}] {:__fennelview pp-list}))

(assert-eq (pp l2) "(:a\n \"a b\"\n [1 2 3]\n {:a (1\n      2\n      3)\n  :b {}})")
(assert-eq (pp {:list l2})
           "{:list (:a\n        \"a b\"\n        [1 2 3]\n        {:a (1\n             2\n             3)\n         :b {}})}")
(assert-eq (pp [l2]) "[(:a\n  \"a b\"\n  [1 2 3]\n  {:a (1\n       2\n       3)\n   :b {}})]")
(assert-eq (pp {:abc [l1]}) "{:abc [(1\n        2\n        3)]}")
(assert-eq (pp l1 {:one-line? true}) "(1 2 3)")
(assert-eq (pp [l1] {:one-line? true}) "[(1 2 3)]")
(assert-eq (pp {:abc [l1]} {:one-line? true}) "{:abc [(1 2 3)]}")

(fn pp-doc-example [t pp options indent]
  (icollect [i v (ipairs t)]
    (let [v (pp v options (+ 10 indent) true)]
      (if (= i 1)          (.. "@my-table[" v)
          (= i (length t)) (.. "          " v "]")
          (..                  "          " v)))))

(local t1 (setmetatable [1 2 3] {:__fennelview pp-doc-example}))
(assert-eq (pp t1) "@my-table[1\n          2\n          3]")
(assert-eq (pp t1 {:one-line? true}) "@my-table[1 2 3]")
(assert-eq (pp {:my-table t1})
           "{:my-table @my-table[1\n                     2\n                     3]}")
(local t1 (setmetatable [{:a [] :b [1 2 3]} 2 3] {:__fennelview pp-doc-example}))
(assert-eq (pp {:my-table t1})
           "{:my-table @my-table[{:a {}\n                      :b [1 2 3]}\n                     2\n                     3]}")
(assert-eq (pp {:my-table t1} {:sequential-length 1})
           "{:my-table @my-table[{:a {}\n                      :b [1\n                          2\n                          3]}\n                     2\n                     3]}")
(assert-eq (pp {:my-table t1} {:one-line? true})
           "{:my-table @my-table[{:a {} :b [1 2 3]} 2 3]}")

(fn pp-symbol [t pp options indent]
  t)

(local s1 (setmetatable ["a"] {:__fennelview pp-symbol}))

(assert-eq (pp [s1 "a"] {:one-line? true})
           "[a \"a\"]")
